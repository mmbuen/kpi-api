﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPI.Services.Models
{
    public class ODBCConnectionModel
    {
        public OdbcConnection Connection { get; set; }
        public OdbcDataReader Reader { get; set; }
    }

    public class SQLConnectionModel
    {
        public SqlConnection Connection { get; set; }
        public SqlDataReader Reader { get; set; }
    }

}
