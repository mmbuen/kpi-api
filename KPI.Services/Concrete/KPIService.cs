﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KPI.Common.Enums;
using KPI.Common.ResponseModels;
using KPI.Services.Abstract;
using KPI.Services.Helpers;

namespace KPI.Services.Concrete
{
	public class KPIService : IKPIService
	{
		public async Task<List<KPIResponse>> getKPIDetails()
		{
			var list = new List<KPIResponse>();
			string query = "SELECT * FROM View_KPI_Details";
			var config = Connections.SQLConnect(query, default);
			if (config.Reader.HasRows)
			{
				var prevPerfInd = "";
				List<Component> comp = new List<Component>();
				while (config.Reader.Read())
				{
					var period = config.Reader["Period"].ToString();
					var agmgroup = config.Reader["AGM_Group"].ToString();
					var dept = config.Reader["Department"].ToString();
					var critsort = config.Reader["CriteriaSort"].ToString();
					var critname = config.Reader["CriteriaName"].ToString();
					var mfo = config.Reader["MFO"].ToString();
					var perfind = config.Reader["Performance_Indicator"].ToString();
					var figure = config.Reader["Figure"].ToString();

					if (prevPerfInd != perfind)
                    {
						comp = new List<Component>();
						list.Add(new KPIResponse
						{
							Period = period,
							AGMGroup= agmgroup,
							Dept= dept,
							CritSort = critsort,
							CritName= critname,
							Mfo= mfo,
							PerfInd= perfind,
							Figure= figure,
							Components=comp,
						});
					}

					comp.Add(new Component
					{
						CompName = config.Reader["ComponentName"].ToString(),
						FigureComp = config.Reader["FigureComponent"].ToString(),
						 FigureYTD = config.Reader["FigureYTD"].ToString(),

					});

					prevPerfInd = perfind;
				}
				list = list.OrderBy(q => q.CritSort).ToList();
			}
			config.Connection.Close();
			return list;
		}
	}
}
