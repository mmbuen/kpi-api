﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KPI.Common.ResponseModels;
using KPI.Services.Abstract;
using KPI.Services.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace KPI.Services.Concrete
{
	public class DepartmentService : IDepartmentService
	{

		public async Task<List<DepartmentResponse>> getDepartment()
		{
			var list = new List<DepartmentResponse>();
			
			
			string query = "SELECT B.DepartmentCode as AGMCode, B.Acronym as AGMGroup, A.DepartmentCode,A.Acronym, A.[Description] from Department_KPI A join Department_KPI B on A.AGGroup=B.DepartmentCode order by B.Acronym";
			var config = Connections.SQLConnect(query, default);

			if (config.Reader.HasRows)
			{
				var prevAgmGroup = "";
				List<Department> agm = new List<Department>();
				while (config.Reader.Read())
				{
					var agmcode = config.Reader["AGMCode"].ToString();
					var agmgroup = config.Reader["AGMGroup"].ToString();

					if (prevAgmGroup != agmgroup)
                    {
						agm = new List<Department>();
						list.Add(new DepartmentResponse
						{
							AGMCode = agmcode,
							AGMGroup = agmgroup,
							Departments = agm,

						}) ;
						
					}
					agm.Add(new Department
					{
						AGMGroupDept = agmgroup,
						DepartmentCode = config.Reader["DepartmentCode"].ToString(),
						Acronym = config.Reader["Acronym"].ToString(),
						Description = config.Reader["Description"].ToString(),
					});

					prevAgmGroup = agmgroup;

				}


				list = list.OrderBy(q => q.AGMGroup).Distinct().ToList();
				//agm = agm.OrderBy(q => q.DepartmentCode).ToList();


			}
			config.Connection.Close();
			return list;
		}
	}
}
