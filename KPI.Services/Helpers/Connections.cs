﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KPI.Common.Constants;
using KPI.Common.Enums;
using KPI.Services.Models;

namespace KPI.Services.Helpers
{
	public class Connections
	{
        public static ODBCConnectionModel ODBCConnect(string query, ODBCConnectionType type)
        {
            string connectionString;
            switch (type)
            {
                
                default:
                    connectionString = "";
                    break;
            }
            OdbcConnection connection = new OdbcConnection(connectionString);
            connection.Open();
            OdbcCommand command = new OdbcCommand(query, connection);
            return new ODBCConnectionModel
            {
                Reader = command.ExecuteReader(),
                Connection = connection
            };
        }

        public static SQLConnectionModel SQLConnect(string query, SQLConnectionType type)
        {
            string connectionString;
            switch (type)
            {
                default:
                    connectionString = ConnectionStrings.Databank;
                    break;
            }
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            SqlCommand command = new SqlCommand(query, sqlConnection);
            return new SQLConnectionModel
            {
                Reader = command.ExecuteReader(),
                Connection = sqlConnection
            };
        }

    }
}
