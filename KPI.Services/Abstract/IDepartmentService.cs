﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KPI.Common.ResponseModels;

namespace KPI.Services.Abstract
{
	public interface IDepartmentService
	{
		Task<List<DepartmentResponse>> getDepartment();
	}
}
