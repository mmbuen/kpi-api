﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPI.Common.ResponseModels
{
    public class DepartmentResponse 
    {
        public string AGMGroup { get; set; }

        public string AGMCode { get; set; }

        public List<Department> Departments { get; set; }


    }

    public class Department
    {
        public string AGMGroupDept { get; set; }
        public string DepartmentCode { get; set; }
        public string Acronym { get; set; }
        public string Description { get; set; }
    }
}
