﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPI.Common.ResponseModels
{
	public class KPIResponse
	{
		//public List<KPIResponseModel> list { get; set; }
		public string Period { get; set; }
		public string AGMGroup { get; set; }
		public string Dept { get; set; }

		public string CritSort { get; set; }
		public string CritName { get; set; }
		public string Mfo { get; set; }
		public string PerfInd { get; set; }
		public string Figure { get; set; }
		public List<Component> Components { get; set; }
		//public string FigureComp { get; set; }
		//public string FigureYTD { get; set; }
	}

	public class Component
    {
		public string CompName { get; set; }
		public string FigureComp { get; set; }
		public string FigureYTD { get; set; }
	}
}
